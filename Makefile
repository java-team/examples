help:
	make test-$$NAME to test that the package builds
	make clean-$$NAME to clean it

test-%:
	tar zcf $*_1.0.orig.tar.gz $*
	(cd $* ;\
	 dpkg-buildpackage -b)

clean-%:
	(cd $* ;\
	 fakeroot debian/rules clean)
	rm -f $*_1.0.orig.tar.gz
	rm -f *.deb *.changes
