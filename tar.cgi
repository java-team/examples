#!/bin/sh --

echo "Content-type: application/x-gzip"
echo ""

if [ "`basename "$PATH_INFO"`" != "`basename "$PATH_INFO" .tar.gz`" ]; then
	EX="`basename "$PATH_INFO" .tar.gz`"
	if [ -d "$EX" ]; then
		tar cz "$EX"
	fi
fi
